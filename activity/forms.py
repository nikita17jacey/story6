from django import forms
import datetime
class ActForm(forms.Form):
        activity = forms.CharField(widget=forms.TextInput(attrs={
            'class' : 'form-control',
            'placeholder' : 'Activity Name',
            'type' : 'text',
            'required' : True,
        }))
        date = forms.DateTimeField(widget=forms.DateTimeInput(attrs={
            'class' : 'form-control',
            'placeholder' : 'Date',
            'type' : 'datetime-local',
            'required' : True,
        }))
        place = forms.CharField(widget=forms.TextInput(attrs={
            'class' : 'form-control',
            'placeholder' : 'Place',
            'type' : 'text',
            'required' : True,
        }))
        description= forms.CharField(widget=forms.Textarea(attrs={
            'class' : 'form-control',
            'placeholder' : 'Description',
            'type' : 'text',
            'required' : True,
        }))

class UserForm(forms.Form):
        user = forms.CharField(widget=forms.TextInput(attrs={
            'class' : 'form-control',
            'placeholder' : 'User Name',
            'type' : 'text',
            'required' : True,
        }))

        email = forms.EmailField(widget=forms.EmailInput(attrs={
            'class' : 'form-control',
            'placeholder' : 'User Email ',
            'type' : 'text',
            'required' : True,
        }))
