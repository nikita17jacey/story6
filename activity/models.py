from django.db import models

# Create your models here.
class Activity(models.Model):
    name= models.CharField(max_length= 50)
    time = models.DateTimeField()
    place= models.CharField(max_length= 100, default= 1)
    description = models.TextField(max_length= 300)
    def __str__(self):
        return self.name

class Member(models.Model):
    act_user= models.ForeignKey(Activity,  on_delete= models.CASCADE, default = 1)
    user = models.CharField(max_length = 30)
    email = models.EmailField(max_length = 50)
    def __str__(self):
        return self.user