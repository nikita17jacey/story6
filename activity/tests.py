from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import Join, join_me
from .models import Activity, Member
from .forms import ActForm, UserForm

class Story6Test(TestCase):
    def test_story6_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_story6_using_detail_func(self):
        found = resolve('/story6/1/')
        self.assertEqual(found.func, detail)

    def test_story6_model_can_add(self):
        Activity.objects.create(name='test', time='2002-05-01 12:00', place = 'belyos', description='this is a test')
        hitungjumlah = Activity.objects.all().count()
        self.assertEqual(hitungjumlah, 1)

    def test_story_6_model_add_member(self):
        Activity.objects.create(name='test')
        test_act = Activity.objects.get(name='test')
        User.objects.create(test_act = name, member='test')
        hitungjumlah = User.objects.all().count()
        self.assertEqual(hitungjumlah, 1)

    def test_form_validation_for_blank_items(self):
        form = ActForm(data={'name':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'],
            ["This field is required."]
        )

