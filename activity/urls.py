# from django.urls import path
# from . import views
#
# urlpatterns = [
#     path('', views.activity, name = 'activity'),
# ]

from django.urls import path
from .views import Join, join_me

urlpatterns = [
    path('', Join, name = 'Activity'),
    path('<int:pk>/', join_me, name = 'join_me')

]
